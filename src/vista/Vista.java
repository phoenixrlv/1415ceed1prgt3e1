/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Alumno;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Vista {

  public Alumno getAlumno() {

    InputStreamReader isr = new InputStreamReader(System.in);
    BufferedReader br = new BufferedReader(isr);

    Alumno a;
    String nombre = "";
    String linea = "";
    int edad;

    System.out.println("ENTRADA DE DATOS.");
    System.out.print("Nombre: ");
    try {
      nombre = br.readLine();
    } catch (IOException ex) {
      Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
    }

    System.out.print("Edad: ");
    try {
      linea = br.readLine();
    } catch (IOException ex) {
      Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
    }

    edad = Integer.parseInt(linea);

    a = new Alumno(nombre, edad);

    return a;
  }

  public void showAlumno(Alumno a) {
    System.out.println("DATOS ALUMNO.");
    System.out.println("Nombre: " + a.getNombre());
    System.out.println("Edad: " + a.getEdad());
  }

}
